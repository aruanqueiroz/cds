{
    'name' : "CheiroDeShangue",
    'version' : "1.0",
    'description' : "É o Cheiro que chama a atenção do lobo",
    'author' : "Aruan Queiroz, OdooClass.com",
    'depends' : ['sale'],
    'data' : ['cds_view.xml', 'security/ir.model.access.csv'],
    'installable' : True,
}
